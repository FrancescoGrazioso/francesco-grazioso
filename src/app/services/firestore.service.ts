import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private afs: AngularFirestore) { }

  add(object: any, path: string): any {
    this.afs.collection(path).add(object)
      .then(
        d => {
          console.log(d);
          object.key = d.id;
          this.afs.collection(path).doc(d.id).update(object);
        }
      )
      .catch(e => { console.log(e); });
  }


  update(path: string, key: string, data: any) {
    this.afs.collection(path).doc(key).update(data);
  }

  delete(path: string, key: string) {
    this.afs.collection(path).doc(key).delete();
  }
}
