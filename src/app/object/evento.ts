export interface Evento {
    nomeEvento: string;
    provincia: string;
    citta: string;
    dataInizio: Date;
    dataFine: Date;
    key: string;
    prezzo: string;
    descrizione: string;
    telefono: string;
    email: string;
    copertina: string[];
}
