export interface Cliente {
  uid: string;
  displayName: string;
  email: string;
  cellulare: string;
  indirizzo: string;
  key: string;
  isSuperutente: boolean;
  photoURL: string;
  emailVerified: boolean;
}
