import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  errorMessage: string;
  successMessage: string;
  email: string;
  password: string;
  load = false;

  constructor(private authService: AuthService,
              private deviceService: DeviceDetectorService) { }

  ngOnInit() {
  }

  sighUp() {
    this.load = true;
    this.authService.SignUp(this.email, this.password)
        .then(
            d => {this.load = false; }
        ).catch(
        d => {this.load = false; }
    );
  }

  signUpWithGoogle() {
    this.authService.GoogleAuth();
  }

  isDesktop() {
    const isDesktopDevice = this.deviceService.isDesktop();
    return isDesktopDevice;
  }


}
