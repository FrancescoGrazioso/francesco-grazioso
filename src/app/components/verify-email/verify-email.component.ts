import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {
    mode: string;
    actionCode: string;

  constructor(
      private authService: AuthService,
      private afAuth: AngularFireAuth,
      private route: ActivatedRoute,
      private router: Router
  ) { }

    ngOnInit() {
        this.route.queryParams
            .subscribe(params => {
                this.mode = params.mode;
                this.actionCode = params.oobCode;
                if (this.mode === 'verifyEmail') {
                    this.handleVerifYMail(this.actionCode);
                }
            });
    }

    handleVerifYMail(oobCode: string) {
        this.afAuth.auth.applyActionCode(oobCode)
            .then(
                r => {
                    console.log(r);
                    if (confirm('La tua mail è stata verificata con successo!')) {
                        this.router.navigate(['profile']);
                    }
                }
            )
            .catch(
                e => {
                    alert('C \' stato un problema nel verificare la tua mail, ti preghiamo di richiederne una nuova');
                }
            );
    }

  sendVerificationMailAgain() {
    this.authService.SendVerificationMail().then(
        d => {
          alert('Una nuova mail di conferma è stata inviata');
        }
    ).catch(
        d => {
          alert('Ci sono stati problemi nell\' invio della mail ');
        }
    );
  }

}
