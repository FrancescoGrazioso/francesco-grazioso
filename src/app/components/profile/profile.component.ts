import {Component, ElementRef, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Cliente} from '../../object/cliente';
import {AngularFirestore} from '@angular/fire/firestore';
import {Evento} from '../../object/evento';
import {DeviceDetectorService} from 'ngx-device-detector';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: Cliente = JSON.parse(localStorage.getItem('user'));
  load = false;

  evento: Evento;
  eventi: Evento[] = [];

  constructor(
      private deviceService: DeviceDetectorService,
      private afs: AngularFirestore,
      private authService: AuthService,
      private router: Router
  ) { }

  ngOnInit() {
    this.load = true;
    this.afs.collection<Evento>('events', ref => ref.where('email', '==', this.user.email)).valueChanges().subscribe(
        ev => {
          this.eventi = [];
          for (let i = 0; i < ev.length; i++) {
            this.eventi.push(ev[i]);
          }
          this.load = false;
        }
    );

    if (this.user.uid !== this.user.key) {
      this.load = true;
      this.afs.doc(`users/${this.user.uid}`).valueChanges().subscribe(
          (u: Cliente) => {
            this.user = u;
            localStorage.setItem('user', JSON.stringify(this.user));
            this.load = false;
          }
      );
    }
  }

  showElements() {
    if (document.getElementById('events_list').classList.contains('active')) {
      return true;
    } else {
      return false;
    }
  }

  switchPage() {
    if (document.getElementById('events_list').classList.contains('active')) {
      document.getElementById('events_list').classList.remove('active');
      document.getElementById('new_event').classList.add('active');
    } else {
      document.getElementById('events_list').classList.add('active');
      document.getElementById('new_event').classList.remove('active');
    }
  }

  sighOut() {
    this.authService.SignOut();
  }

  editUserData() {
    // routerLink="/editUserData"
    this.router.navigate(['/editUserData']);
  }

  isDesktop() {
    const isDesktopDevice = this.deviceService.isDesktop();
    return isDesktopDevice;
  }

  isVertical() {
    const height = window.outerHeight;
    const width = window.outerWidth;

    return (height / width > 1);
  }

}
