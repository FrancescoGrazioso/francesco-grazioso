import {Component, Input, OnInit} from '@angular/core';
import {Cliente} from '../../object/cliente';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-user-data',
  templateUrl: './edit-user-data.component.html',
  styleUrls: ['./edit-user-data.component.css']
})
export class EditUserDataComponent implements OnInit {
  user: Cliente;
  userKey: string;
  load = false;

  constructor(
      private route: ActivatedRoute,
      private afs: AngularFirestore,
      private authService: AuthService
  ) { }

  ngOnInit() {
    this.load = true;
    this.route.queryParams
        .subscribe(params => {
          this.userKey = params.id;
          this.afs.doc(`users/${this.userKey}`).valueChanges().subscribe(
              (u: Cliente) => {
                this.user = u;
                this.load = false;
              }
          );
        });
  }

  saveUserData() {
    this.load = true;
    localStorage.setItem('user', JSON.stringify(this.user));
    this.authService.SetUserData(this.user).then(
        u => {
          localStorage.setItem('user', JSON.stringify(this.user));
          this.load = false;
        }
    );
  }

}
