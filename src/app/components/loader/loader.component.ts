import { Component, OnInit } from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  constructor(private deviceService: DeviceDetectorService) {}


  ngOnInit() {}

  isDesktop() {
    const isDesktopDevice = this.deviceService.isDesktop();
    return isDesktopDevice;
  }

}
