import { Component, OnInit } from '@angular/core';
import {Evento} from '../../object/evento';
import {DeviceDetectorService} from 'ngx-device-detector';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  load = false;
  eventi: Evento[];

  constructor(private deviceService: DeviceDetectorService,
              private afs: AngularFirestore) {
    this.isDesktop();
  }

  ngOnInit() {
    this.load = true;
    this.afs.collection<Evento>('events').valueChanges().subscribe(
        ev => {
          this.eventi = [];
          for (let i = 0; i < ev.length; i++) {
            this.eventi.push(ev[i]);
          }
          this.load = false;
        }
    );
  }

  isDesktop() {
    const isDesktopDevice = this.deviceService.isDesktop();
    return isDesktopDevice;
  }

  isVertical() {
      const height = window.outerHeight;
      const width = window.outerWidth;
      return (height / width > 1);
  }

}
