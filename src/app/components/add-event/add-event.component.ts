import {Component, Input, OnInit} from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';
import {Evento} from '../../object/evento';
import {Cliente} from '../../object/cliente';
import {AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask} from '@angular/fire/storage';
import {Observable} from 'rxjs';
import {FirestoreService} from '../../services/firestore.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit {
  @Input() user: Cliente;
  copertina: File[];
  load = false;
  eventoNuovo: Evento;
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable<number>;
  uploadDone = false;
  toUpload = false;

  constructor(
      private deviceService: DeviceDetectorService,
      private afStorage: AngularFireStorage,
      private afs: AngularFirestore,
      private router: Router
  ) {}

  ngOnInit() {
    const dataInizio: Date = new Date();
    const dataFine: Date = new Date();
    dataFine.setMonth(dataInizio.getMonth() + 1)

    this.eventoNuovo = {
      nomeEvento : '',
      provincia: '',
      citta: '',
      dataInizio,
      dataFine,
      key: '',
      prezzo: '',
      descrizione: '',
      telefono: '',
      email: this.user.email,
      copertina: [],
    };

  }

  uploadCopertina() {
    this.uploadDone = false;
    this.load = true;
    const id = this.eventoNuovo.key;
    this.ref = this.afStorage.ref(id + '/copertina'  );
    for (let i = 0; i < this.copertina.length; i++) {
      this.task = this.ref.put(this.copertina[i]);
    }
    this.uploadProgress = this.task.percentageChanges();
    this.task.then(
        d => {
          this.ref.getDownloadURL().subscribe(
              url => {
                this.eventoNuovo.copertina.push(url);
                this.afs.collection('events').doc(this.eventoNuovo.key).update(this.eventoNuovo);
              }
          );
        }
    ).finally(
        () => {
          this.uploadDone = true;
          this.load = false;
          this.router.navigate(['profile']);
          window.location.reload();
        }
    );
  }

  onFileChange(event) {
    this.copertina = event.target.files;
  }

  caricaEvento() {
    const path = 'events';
    this.afs.collection(path).add(this.eventoNuovo)
        .then(
            d => {
              console.log(d);
              this.eventoNuovo.key = d.id;
              this.uploadCopertina();
            }
        )
        .catch(e => { console.log(e); });
  }

  delete(path: string) {
    this.afStorage.ref(path).delete();
  }

}

