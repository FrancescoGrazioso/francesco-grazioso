import {Component, OnInit, Renderer} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorMessage: string;
  successMessage: string;
  email: string;
  password: string;
  load = false;

  constructor(
      public authService: AuthService,
      private deviceService: DeviceDetectorService,
      private render: Renderer
  ) { }

  signInWithMail(email: string, password: string) {
    this.load = true;
    this.authService.SignIn(email, password)
        .then(
        d => {this.load = false; }
    ).catch(
        d => {
          alert(d);
          this.load = false;
        }
    );
  }

  signInWithGoogle() {
    this.authService.GoogleAuth();
  }

  signInWithFacebook() {
    this.authService.FacebookAuth();
  }

  signUp() {
    this.load = true;
    this.authService.SignUp(this.email, this.password)
        .then(
            d => {this.load = false; }
        ).catch(
        d => {this.load = false; }
    );
  }

  signUpWithGoogle() {
    this.authService.GoogleAuth();
  }

  ngOnInit() { }

  isDesktop() {
    const isDesktopDevice = this.deviceService.isDesktop();
    return isDesktopDevice;
  }

  switchForm() {
    this.email = null;
    this.password = null;
    const element = document.getElementById('cont');
    if (element.classList.contains('s--signup')) {
      element.classList.remove('s--signup');
    } else {
      element.classList.add('s--signup');
    }

  }

}
