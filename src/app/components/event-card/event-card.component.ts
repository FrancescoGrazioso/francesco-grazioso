import {Component, Input, OnInit} from '@angular/core';
import {Evento} from '../../object/evento';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.css']
})
export class EventCardComponent implements OnInit {
  @Input() evento: Evento;

  constructor() { }

  ngOnInit() {
  }

}
