import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  mode: string;
  actionCode: string;

  constructor(
      private authService: AuthService,
      private afAuth: AngularFireAuth,
      private route: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams
        .subscribe(params => {
          this.mode = params.mode;
          this.actionCode = params.oobCode;
          if (this.mode === 'resetPassword') {
            this.handleRecoveryPassword(this.actionCode);
          }
        });
  }

  handleRecoveryPassword(oobCode: string) {
    let email = '';
    this.afAuth.auth.verifyPasswordResetCode(oobCode)
        .then(
            r => {
              email = r;
              //show screen to reset password showing emai, then once a new password is created
              this.afAuth.auth.confirmPasswordReset(oobCode, email).then(
                  c => {
                    //handle password reset
                  }
              );
            }
        )
        .catch(
            e => {
              alert('');
            }
        );
  }

}
