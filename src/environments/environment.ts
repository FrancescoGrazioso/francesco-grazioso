// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAzSRVjGQgV8La1O1N6hilxc4cjNasRSug',
    authDomain: 'dovo-4b9ac.firebaseapp.com',
    databaseURL: 'https://dovo-4b9ac.firebaseio.com',
    projectId: 'dovo-4b9ac',
    storageBucket: 'dovo-4b9ac.appspot.com',
    messagingSenderId: '882392358848',
    appId: '1:882392358848:web:a6bcd3e4228af40c'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
