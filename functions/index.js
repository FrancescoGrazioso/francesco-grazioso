/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


/**
 * This database triggered function will check for child nodes that are older than the
 * cut-off time. Each child needs to have a `timestamp` attribute.
 */
exports.deleteOldItems = functions.database.ref('/events/{pushId}').onWrite(async (change) => {
    let now = new Date(); // Maybe use luxon or moment
    now.setMonth(d.getMonth() - 1);
    const query = firestore.collection('items')
      .where('data_fine', '<=', now)
    const snapshots = await query.get();
    // Snapshots now contains the documents to delete
    const batch = firestore.batch();
    snapshots.forEach(v => batch.delete(v.ref));
    await batch.commit();
});
